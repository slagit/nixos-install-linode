{
  description = "Nix Installer";
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.11";
    nixos-common.url = "gitlab:slagit/nixos-common";
  };
  outputs = { self, flake-utils, nixpkgs, nixos-common }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system: let
      pkgs = import nixpkgs {
        inherit system;
      };
    in rec {
      nixosConfigurations.default = nixpkgs.lib.nixosSystem {
        inherit system;
        modules = [
          (builtins.toPath "${nixpkgs}/nixos/modules/installer/cd-dvd/installation-cd-minimal.nix")
          nixos-common.nixosModules.common
          ./install.nix
        ];
      };
      packages = {
        default = packages.image;
        image = nixosConfigurations.default.config.system.build.isoImage;
        setup = pkgs.buildGoModule {
          name = "setup";
          src = ./setup;
          vendorHash = "sha256-d7OIq0jzQUS5+Ja+jE+Bs3BL3OHQxtHJjOPTmBhkirQ=";
        };
      };
    });
}
