{lib, pkgs, ...}: let
  ssh_ca_pub = pkgs.writeText "ca.pub" ''
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGpblcVzzHoULrq+lSGgIKhdVCZNCC9Yj6PlBI6j4LgR slagit-vault.net ssh-client-signer
  '';
in {
  boot = {
    kernelParams = [ "console=ttyS0,192008" ];
    loader.grub = {
      extraConfig = ''
        serial --speed=19200 --unit=0 --word=8 --parity=no --stop=1;
        terminal_input serial;
        terminal_output serial;
      '';
      memtest86.enable = lib.mkForce false;
    };
    supportedFilesystems = lib.mkForce [];
  };
  documentation = {
    enable = lib.mkForce false;
    nixos.enable = lib.mkForce false;
  };
  environment.systemPackages = [
    pkgs.git
  ];
  hardware.enableRedistributableFirmware = lib.mkForce false;
  networking = {
    tempAddresses = "disabled";
    wireless.enable = lib.mkForce false;
  };
  services.openssh.permitRootLogin = lib.mkForce "no";
  system.extraDependencies = lib.mkForce [];
}
