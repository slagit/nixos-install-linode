# NixOS Installer for Linode

## Building

```
nix build
scp result/iso/*.iso cloudy.slagit.net:nixos-install-linode.iso
```

## Installation

1. Create a new Linode

   ```sh
   nix build .#setup

   export LINODE_CLI_TOKEN=....
   ./result/bin/setup <instance_label>
   ```

1. In rescue mode, copy installer

   ```sh
   ssh -t kocha@lish-us-east.linode.com <instance_label>
   curl -k -u albert sftp://cloudy.slagit.net/home/albert/nixos-install-linode.iso | dd of=/dev/sda
   ```

1. In install mode, perform installation

   ```sh
   sv -principal=nixos -role=nixos nixos@192.0.2.16
   sudo e2label /dev/sda nixos
   sudo mount /dev/disk/by-label/nixos /mnt
   sudo nixos-install --flake path/to/repo#hostname
   ```


