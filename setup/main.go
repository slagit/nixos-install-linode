package main

import (
	"context"
	"fmt"
	"net/http"
	"os"

	"github.com/linode/linodego"
	"golang.org/x/oauth2"
)

const InstallDiskSize = 512
const Region = "us-east"
const Type = "g6-nanode-1"

func newClient() linodego.Client {
	token := os.Getenv("LINODE_CLI_TOKEN")
	if token == "" {
		fmt.Println("LINODE_CLI_TOKEN environment variable unset")
		os.Exit(1)
	}
	tokenSource := oauth2.StaticTokenSource(&oauth2.Token{AccessToken: token})
	return linodego.NewClient(&http.Client{
		Transport: &oauth2.Transport{
			Source: tokenSource,
		},
	})
}

func createDisk(ctx context.Context, c linodego.Client, id int, opts linodego.InstanceDiskCreateOptions) (*linodego.InstanceDisk, error) {
	fmt.Printf("Creating %s disk...\n", opts.Label)
	disk, err := c.CreateInstanceDisk(ctx, id, opts)
	if err != nil {
		return nil, err
		panic(err)
	}

	fmt.Println("Waiting for disk to be created...")
	return c.WaitForInstanceDiskStatus(ctx, id, disk.ID, linodego.DiskReady, 120);
}

func createInstance(ctx context.Context, c linodego.Client, opts linodego.InstanceCreateOptions) (*linodego.Instance, error) {
	fmt.Printf("Creating %s instance...\n", opts.Label)
	instance, err := c.CreateInstance(ctx, opts)
	if err != nil {
		return nil, err
	}

	fmt.Println("Waiting for instance to be created...")
	return c.WaitForInstanceStatus(ctx, instance.ID, linodego.InstanceOffline, 120);
}

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("Usage: %s <instance label>\n", os.Args[0])
		os.Exit(1)
	}
	instanceLabel := os.Args[1]

	ctx := context.Background()
	c := newClient()

	ltype, err := c.GetType(ctx, Type)
	if err != nil {
		panic(err)
	}

	instance, err := createInstance(ctx, c, linodego.InstanceCreateOptions{
		Label: instanceLabel,
		Region: Region,
		Type: Type,
	})
	if err != nil {
		panic(err)
	}

	install_disk, err := createDisk(ctx, c, instance.ID, linodego.InstanceDiskCreateOptions{
		Filesystem: "raw",
		Label: "Installer",
		Size: InstallDiskSize,
	})
	if err != nil {
		panic(err)
	}

	os_disk, err := createDisk(ctx, c, instance.ID, linodego.InstanceDiskCreateOptions{
		Filesystem: "ext4",
		Label: "NixOS",
		Size: ltype.Disk - InstallDiskSize,
	})
	if err != nil {
		panic(err)
	}

	fmt.Println("Creating install configuration...")
	installRootDevice := "/dev/sdb"
	install_config, err := c.CreateInstanceConfig(ctx, instance.ID, linodego.InstanceConfigCreateOptions{
		Devices: linodego.InstanceConfigDeviceMap{
			SDA: &linodego.InstanceConfigDevice{
				DiskID: os_disk.ID,
			},
			SDB: &linodego.InstanceConfigDevice{
				DiskID: install_disk.ID,
			},
		},
		Helpers: &linodego.InstanceConfigHelpers {},
		Kernel: "linode/direct-disk",
		Label: "Installer",
		RootDevice: &installRootDevice,
	})
	if err != nil {
		panic(err)
	}

	fmt.Println("Creating os configuration...")
	osRootDevice := "/dev/sda"
	os_config, err := c.CreateInstanceConfig(ctx, instance.ID, linodego.InstanceConfigCreateOptions{
		Devices: linodego.InstanceConfigDeviceMap{
			SDA: &linodego.InstanceConfigDevice{
				DiskID: os_disk.ID,
			},
		},
		Helpers: &linodego.InstanceConfigHelpers {},
		Kernel: "linode/grub2",
		Label: "NixOS",
		RootDevice: &osRootDevice,
	})
	if err != nil {
		panic(err)
	}

	fmt.Println("Booting rescue mode...")
	if err := c.RescueInstance(ctx, instance.ID, linodego.InstanceRescueOptions{
		Devices: linodego.InstanceConfigDeviceMap{
			SDA: &linodego.InstanceConfigDevice{
				DiskID: install_disk.ID,
			},
		},
	}); err != nil {
		panic(err)
	}

	fmt.Println("")
	fmt.Println("")
	fmt.Printf("ssh -t kocha@lish-%s.linode.com %s\n", Region, instanceLabel)
	fmt.Println("curl -k -u albert sftp://cloudy.slagit.net/home/albert/nixos-install-linode.iso | dd of=/dev/sda")
	fmt.Println("")
	fmt.Println("")
	fmt.Println("Press the Enter key once installer has been copied")
	fmt.Scanln()

	fmt.Println("Booting install config...")
	if err := c.RebootInstance(ctx, instance.ID, install_config.ID); err != nil {
		panic(err)
	}

	fmt.Println("")
	fmt.Println("")
	fmt.Printf("sv -principal=nixos -role=nixos nixos@%s\n", instance.IPv4[0].String())
	fmt.Println("")
	fmt.Println("")
	fmt.Println("Press the Enter key once system has been installed")
	fmt.Scanln()

	fmt.Println("Booting os config...")
	if err := c.RebootInstance(ctx, instance.ID, os_config.ID); err != nil {
		panic(err)
	}
}
